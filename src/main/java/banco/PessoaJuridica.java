package banco;

public class PessoaJuridica extends Pessoa {
	private String cnpj;
	private String nomeFantasia;
	public PessoaJuridica(String nome , String cnpj, String nomeFantasia) {
		super(nome);
		this.cnpj=cnpj;
		this.nomeFantasia=nomeFantasia;
		
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public String getCnpj() {
		return cnpj;
	}

}
