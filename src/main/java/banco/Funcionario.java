package banco;

public class Funcionario extends PessoaFisica {
	private Double salario;
	public Funcionario(String nome, String cpf, Double salario) {
		super(nome, cpf);
		this.salario=salario;
	}
	public Double getSalario() {
		return salario;
	}

}
