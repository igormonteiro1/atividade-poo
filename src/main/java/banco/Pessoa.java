package banco;

import java.util.Date;

public class Pessoa {
	private String nome;
	private Date dtanasc;
	private String endereco;
	private String senha;
	public String getSenha() {
		return senha;
	}
	public Pessoa(String nome) {
		super();
		this.nome=nome;
	}
	public String getnome() {
		return nome;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public Date getDtanasc() {
		return dtanasc;
	}
	public void setDtanasc(Date dtanasc) {
		this.dtanasc = dtanasc;
	}
	
}
